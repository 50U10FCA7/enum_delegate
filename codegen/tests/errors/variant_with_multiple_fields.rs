use enum_delegate::delegate;

#[delegate]
enum Name {
    First { first: String, last: String },
}

fn main() {
    unreachable!()
}
